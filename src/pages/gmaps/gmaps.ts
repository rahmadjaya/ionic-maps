import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
/**
 * Generated class for the GmapsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google: any;
@Component({
  selector: 'page-gmaps',
  templateUrl: 'gmaps.html',
})
export class GmapsPage {
	@ViewChild('map') mapRef: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GmapsPage');
    console.log(this.mapRef);
    this.showMap();
  }

  Home(){
  	this.navCtrl.setRoot(HomePage);
  }

  showMap(){
  	const location = new google.maps.LatLng(-7.144512200000000,110.40834579999999);

  	const options ={
  		center : location,
  		zoom : 16
  	}
  	const map = new google.maps.Map(this.mapRef.nativeElement, options);

  	this.addMarker(location, map);
  }

  addMarker(position, map){
  	return new google.maps.Marker({
  		position,
  		map
  	})
  }
}
