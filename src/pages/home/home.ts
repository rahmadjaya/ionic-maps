import { Component } from '@angular/core';
import { NavController, ModalController, ActionSheetController } from 'ionic-angular';
import { LokasiPage } from '../lokasi/lokasi';
import { PlacePage } from '../place/place';
import { GmapsPage } from '../gmaps/gmaps';
import { Geolocation } from '@ionic-native/geolocation';
import { MapProvider } from '../../providers/map/map';
import { Place } from '../../model/place.model';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	lokasi : {title:string}[] =[];

  constructor(private actionCtrl : ActionSheetController,private modalCtrl: ModalController,private mapService : MapProvider,public navCtrl: NavController, public geolocation: Geolocation) {

  }

  ionViewWillEnter(){
  	this.mapService.getLokasi()
  		.then((res)=>this.lokasi = res);
  }

  onNewLokasi(){
  	this.navCtrl.push(LokasiPage);
  }

  openPlace(data : Place){
  	this.modalCtrl.create(PlacePage, data).present();
  }

  actionShare(data){
    let share = this.actionCtrl.create({
      title : "SHARE LOCATION",
      buttons : [
        {
          text : "Share",
          icon : "logo-twitter",
          role : "share",
          handler: ()=>{
              console.log('share twitter')
              console.log(data);
          }
        },
        {
          text : "Cancel",
          role : 'cancel'
        }
      ]
    });
    share.present();
  }

  Gmaps(){
    this.navCtrl.setRoot(GmapsPage);
  }
}