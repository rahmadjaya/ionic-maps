import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MapProvider } from '../../providers/map/map';
import { Geolocation } from '@ionic-native/geolocation';


/**
 * Generated class for the LokasiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-lokasi',
  templateUrl: 'lokasi.html',
})
export class LokasiPage {
	location :{lat:number, lng : number} = {lat:0, lng:0};
	
  constructor(public geolocation: Geolocation, private mapService: MapProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LokasiPage');
  }

  onAddLokasi(value: {title:string}){
  	this.mapService.addLokasi({title : value.title, location: this.location});
  	this.navCtrl.pop();
  }

  onLokasiUser(){
  	this.geolocation.getCurrentPosition()
  		.then(
  			(res)=>{
          this.location.lat = res.coords.latitude;
          this.location.lng = res.coords.longitude;
  				console.log('sukses')

  				// this.location = res;
  			}
  		)
  		.catch(
  			(error)=>console.log('Error')
  		);
  }
}
