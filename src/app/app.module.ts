import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { AgmCoreModule } from '@agm/core';
import { HomePage } from '../pages/home/home';
import { LokasiPage } from '../pages/lokasi/lokasi';
import { PlacePage } from '../pages/place/place';
import { GmapsPage } from '../pages/gmaps/gmaps';
import { MapProvider } from '../providers/map/map';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LokasiPage,
    PlacePage,
    GmapsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyANTXrT48d_JM6-U7MuEm-SOGBqWHrFk5k'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LokasiPage,
    PlacePage,
    GmapsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MapProvider
  ]
})
export class AppModule {}
