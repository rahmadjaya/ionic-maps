import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

import { Place } from '../../model/place.model';
/*
  Generated class for the MapProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MapProvider {
	private lokasi : Place[] =[];
  constructor(public http: Http, private storage : Storage) {
    console.log('Hello MapProvider Provider');
  }
  addLokasi(place : Place){
  	this.lokasi.push(place);
  	this.storage.set('lokasi', this.lokasi);
  }
  getLokasi(){
  	return this.storage.get('lokasi')
  		.then((res)=>{
  			this.lokasi = res == null ? [] : res;
  			return this.lokasi.slice();
  		})
  }
}
